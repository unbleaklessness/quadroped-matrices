classdef quadpod_planar_VC   %<   handle   % ( only one instance of this class can exist )
    %FIVELINKS_VC_MJX 5-links robot for MuJoCo with HZD walking controller
        
    properties
        g = 9.82, ...%
        % lengths of links 
        l, ...
        % masses of links
        m, ... 
        % radii of cylindrical links
        r,... = 0.02; %  
        % centers of mass of each link
        c, %
        % moments of inertia of each link
        J, %        
 
        
        leg_stay, % 
        leg_stay_naive, %
        t_step, %
        
        Ks, % P- part of controller
        Kv, % D- part of controller
        Ktvc, %  redundant shit
        
        q, % current state vector
        u, % control vector
        qa, % FROST-style state vector 
        q0, % initial state vector
        t_final, % final simulation time
        curr_t, %      current simulation time
        model, % model path
        
        ctrl, % controller structure
        kk, % controller number
%         q_plus, %
%         q_minus, %
%         alpha, %
        theta = 0, %
        dtheta = 0, %
        theta_max, %
        
        thetadesdata, %
        
        motion_const_data, %
       
        steps_count,%  
       
        RESHUF = [1, 4, 5, 2, 3, 8, 9, 6, 7] % new order of coordinates in case of reshuffling
        
    end
    
    methods
      
        function [M, Cg] = alt_matr(obj)
            M = M_matrix_mex(obj.J, obj.m, obj.c, obj.l, obj.qa);
            Cg = Cg_matrix_mex(obj.J, obj.g, obj.c, obj.m, obj.l, obj.qa);
        end
       
        function obj = J_calcs(obj)
            % J_CALCS calculates vector of inertia moments
            obj.J = zeros(size(obj.l,1),1);
            for ii = 1 : size(obj.l,1)
                obj.J(ii) = obj.m(ii)*(obj.l(ii)^2)*1/12 + obj.m(ii)*(obj.r^2)/4;   
            end
        end
         
        function obj = COM_calcs(obj)
            % COM_CALCS calculates center of mass coordinates for each link
            obj. c = zeros(size(obj.l,1),1);
            for ii = 1 : size(obj.l,1)
                obj. c(ii) = obj.l(ii)/2;   
            end
        end
        
       
        
        function obj = start(obj)
            % START starts MJX simulation of "MODEL" model
            mjx('load', obj.model);    
            Q = obj.vec2struct();

            mjx('set', 'qpos', [Q.x  0  Q.z  angle2quat(0, Q.q1, 0)  Q.q2  Q.q3  Q.q4  Q.q5 Q.q6  Q.q7  Q.q8  Q.q9]);
            mjx('set', 'qvel', [ Q.x_d 0 Q.z_d  0  Q.q1_d  0  Q.q2_d  Q.q3_d  Q.q4_d  Q.q5_d Q.q6_d  Q.q7_d  Q.q8_d  Q.q9_d]);
            mjx('forward');
        end  
        
        function obj = step(obj)
            %STEP forces MJX simulation to calculate and visualize next step 
            
             obj = obj. getState();
             temp = obj.theta;
             obj = obj. get_qa();
             obj = obj. reshuffle_qa();  
             c = [ 1, 0.5, 0, 0, 0, 0, 0, 0];   % these values are the choice vectors for the state variable 
             
             obj.theta = c*obj.qa(2:9,1); % current mechanical phase
             
             obj.dtheta = c*obj.qa(2:9,2); % current mechanical phase speed
             
             obj = obj. VC_PD_control();
%              obj = obj. VC_transversal_control();
             obj = obj. reshuffle_u();             

             obj = obj.ctrlSwitch(temp);            

             % pushing the control forces to the simulation model
             limit = 2000; % saturation of actuation forces/torques             
              disp('^^^^^^^^^^^^^^^^')
             obj.u(1:8) = min(limit , max(-limit , obj. u(1:8)));
             %limiting output torque
   
             mjx('set','ctrl', [(obj. u(1:8))']);
            % producing dynamics simulation
             mjx('step');               
        end       
        
      function  Qs = vec2struct(obj)
            % VEC2STRUCT converts state matrix Q into structure QS
            % coordinates convertion
            Qs. x = obj.q(10, 1);
            Qs. z = obj.q(11, 1);
            Qs. q1 = obj.q(1, 1);
            Qs. q2 = obj.q(2, 1);
            Qs. q3 = obj.q(3, 1);
            Qs. q4 = obj.q(4, 1);
            Qs. q5 = obj.q(5, 1);
            Qs. q6 = obj.q(6, 1);
            Qs. q7 = obj.q(7, 1);
            Qs. q8 = obj.q(8, 1);
            Qs. q9 = obj.q(9, 1);
            
            
            % velocities convertion
            Qs. x_d = obj.q(10, 2);
            Qs. z_d = obj.q(11, 2);
            Qs. q1_d = obj.q(1, 2);
            Qs. q2_d = obj.q(2, 2);
            Qs. q3_d = obj.q(3, 2);
            Qs. q4_d = obj.q(4, 2);
            Qs. q5_d = obj.q(5, 2);  
            Qs. q6_d = obj.q(6, 2);
            Qs. q7_d = obj.q(7, 2);
            Qs. q8_d = obj.q(8, 2);
            Qs. q9_d = obj.q(9, 2); 
        end


        function obj = getState(obj)
            % GETSTATE updates information about current state of multi-link
            % object
            
            temp = mjx('get','qpos');
            temp(4 : 7) = [quat2eul(temp(4 : 7),'ZYX'), -5]; % converting quaternion of body to euler angles 
%             temp(4 : 7) =  [quat2eul(temp(4 : 7),'XYZ'), -5]; % converting quaternion of body to euler angles 
 
            % q1 q2 q3 q4 q5 q6 q7 q8 q9 x z
            obj. q(1 : 11, 1) = [temp(5) ; temp(8) ; temp(9) ; temp(10) ; temp(11) ; temp(12) ; temp(13) ; temp(14) ; temp(15) ; temp(1) ; temp(3)];
%             obj. q(1 : 10, 1) = [temp(5) ; temp(12) ; temp(13) ; temp(14) ; temp(15) ; temp(1) ; temp(3) ; temp(4) ; temp(8) ; temp(9)];

            temp = mjx('get','qvel');
            % q1_d q2_d q3_d q4_d q5_d q6_d q7_d q8_d q9_d x_d z_d
            obj. q(1 : 11, 2) = [temp(5) ; temp(7) ; temp(8) ; temp(9) ; temp(10) ; temp(11) ; temp(12) ; temp(13) ; temp(14) ; temp(1) ; temp(3)];  
                        
            temp = mjx('get','sensordata');
            obj. leg_stay =  [temp(9) temp(10) temp(11) temp(12)] ~= 0; % touch detection
%             disp(obj. leg_stay);
        end
        
        function obj = get_qa(obj)
            % GET_QA calculates transformation from MuJoCo coordinates to FROST coordinates          
           
            obj. qa(1) = obj.q(1);
            obj. qa(2) = pi + obj.q(2);
            obj. qa(3) = obj.q(3);
            obj. qa(4) = pi + obj.q(4);
            obj. qa(5) = obj.q(5);
            obj. qa(6) = pi + obj.q(6);
            obj. qa(7) = obj.q(7);
            obj. qa(8) = pi + obj.q(8);
            obj. qa(9) = obj.q(9);
            obj. qa(1,2) = obj.q(1,2);
            obj. qa(2,2) = obj.q(2,2);
            obj. qa(3,2) = obj.q(3,2);
            obj. qa(4,2) = obj.q(4,2);
            obj. qa(5,2) = obj.q(5,2);
            obj. qa(6,2) = obj.q(6,2);
            obj. qa(7,2) = obj.q(7,2);
            obj. qa(8,2) = obj.q(8,2);
            obj. qa(9,2) = obj.q(9,2);
%             disp('obj.qa');
%             disp(obj.qa);
%             pause;
        end   
        
        function obj = reshuffle_qa(obj)
            %GET_QA calculates transformation from MuJoCo coordinates to FROST coordinates          
                       
            % finding x-coordinates of both legs end-effectors
            xw1 = obj.q(10) - obj.l(2)*sin(obj.q(2)) - obj.l(3)*sin(obj.q(2)+obj.q(3));
            xw2 = obj.q(10) - obj.l(4)*sin(obj.q(4)) - obj.l(5)*sin(obj.q(4)+obj.q(5));            
                
            % ignoring the flight phase
            if sum(obj.leg_stay(1:2)) ~= 0                
                    obj.leg_stay_naive = obj.leg_stay;                    
            end          

         %% reshuffling procedure based on the rule that front leg is
            % always the support leg
            switch obj.leg_stay_naive(1)
                case 0
                    switch obj.leg_stay_naive(2)
                        case 0
                            % never executes
                        case 1 
                           obj. qa(:, 1) = obj. qa(obj.RESHUF, 1);
                           obj. qa(:, 2) = obj. qa(obj.RESHUF, 2);
%                            disp('reshuffle');
                    end
                case 1
                    switch obj.leg_stay_naive(2)
                        case 0
                            %
                        case 1
                            if xw1 < xw2  % the right leg is front
                               obj. qa(:, 1) = obj. qa(obj.RESHUF, 1);
                               obj. qa(:, 2) = obj. qa(obj.RESHUF, 2);
%                                disp('reshuffle');
                            end
                    end
            end                
            
        end           
        
        

        function [ h, h_q, h_qq ] = VC_generation(obj)
            %VC_GENERATION calcuates virtual constrains based on
            %pre-computed polynomial coefficients
          
            c = [ 1, 0.5, 0, 0, 0, 0, 0, 0, 0];   % these values are the choice vectors for the state variable 
           
            M = 5; % Bezie polynom order
            theta_plus = c*obj.ctrl(obj.kk).q_plus; % mechanical phase after interaction
            theta_minus = c*obj.ctrl(obj.kk).q_minus;            

            s = (obj.theta - theta_plus)/(theta_minus - theta_plus); % relational mechanical phase

        %% calculation of Bezier coefficients
            obj.ctrl(obj.kk).alpha_d = zeros(8,M);            
         
            for i = 1:M
                obj.ctrl(obj.kk).alpha_d(:,i) = obj.ctrl(obj.kk).alpha(:,i+1) - obj.ctrl(obj.kk).alpha(:,i);
            end

            obj.ctrl(obj.kk).alpha_dd = zeros(8,M-1);
            
            for i = 1:M-1
                obj.ctrl(obj.kk).alpha_dd(:,i) = obj.ctrl(obj.kk).alpha(:,i+2) - 2*obj.ctrl(obj.kk).alpha(:,i+1) + obj.ctrl(obj.kk).alpha(:,i);
            end

            b = zeros(M+1,1);
            
            for k = 0:M
                b(k+1) = (factorial(M)/(factorial(k)*factorial(M-k)))*s^k*(1-s)^(M-k);
            end

            b_d = zeros(M,1);
            
            for k = 0:M-1
                b_d(k+1) = (factorial(M)/(factorial(k)*factorial(M-k-1)))*s^k*(1-s)^(M-k-1);
            end

            b_dd = zeros(M-1,1);
            
            for k = 0:M-2
                b_dd(k+1) = (factorial(M)/(factorial(k)*factorial(M-k-2)))*s^k*(1-s)^(M-k-2);
            end
            
        %% final Bezier coefficients
            h = obj.ctrl(obj.kk).alpha*b;
            h_q = obj.ctrl(obj.kk).alpha_d*b_d/(c*(obj.ctrl(obj.kk).q_minus - obj.ctrl(obj.kk).q_plus));
            h_qq = obj.ctrl(obj.kk).alpha_dd*b_dd/(c*(obj.ctrl(obj.kk).q_minus - obj.ctrl(obj.kk).q_plus))^2;           
        end    
        
        function [ obj ] = VC_PD_control(obj)
            %VC_CONTROL computes PD-control for joints based on calculated
            %virtual constraints
            
             H0 = [eye(8)];
             c = [1, 0.5, 0, 0, 0, 0, 0, 0];   % these values are the choice vectors for the state variable 
            
             % inertial and forces matrixes
             [M, C] = obj. alt_matr();
             [h, h_q, h_qq] =  obj. VC_generation();      

             h = H0*obj.qa(2:9, 1) - h;
             h_q = H0 - h_q*c;
             h_qq = -h_qq*c*obj. qa(2:9, 2)*c;
             
             B = eye(8);             

             obj. u =0*([h_qq, h_q] * [obj.qa(2:9,2); M\(-C)]);%+ obj.ctrl(obj.kk).Kv(2 : 9, 2 : 9) * h_q * obj.qa(2 : 9, 2) + obj.ctrl(obj.kk).Ks(2 : 9, 2 : 9) * h);
            
             obj. u = -(h_q * (M\ B))\obj.u;    


%             disp(obj.u);

          
        end
         
        function obj = reshuffle_u(obj)
            %RESHUFFLE _U remixes the order of elements of control vector U

            % finding x-coordinates of both legs end-effectors
            xw1 = obj.q(10) - obj.l(2)*sin(obj.q(2)) - obj.l(3)*sin(obj.q(2)+obj.q(3));
            xw2 = obj.q(10) - obj.l(4)*sin(obj.q(4)) - obj.l(5)*sin(obj.q(4)+obj.q(5));            
            
         %% reshuffling procedure based on the rule that front leg is
            % always the support leg
             switch obj.leg_stay_naive(1)
                case 0
                    switch obj.leg_stay_naive(2)
                        case 0
                            % never executes
                        case 1
                        	obj. u = obj.u(obj.RESHUF(2:9)-1); 
                    end
                case 1
                    switch obj.leg_stay_naive(2)
                        case 0
                            %
                        case 1
                            if xw1 < xw2 % the right leg is front
                            	obj. u = obj.u((obj.RESHUF(2:9))-1); 
                            end
                    end
             end          
        end
        
        function obj = ctrlSwitch(obj,temp)
        %CTRLSWITCH switches between available trajectories based on some rule    
            
             % calculating finished steps   
             if (obj.theta < temp) && (obj.theta < pi)
                 obj. steps_count = obj.steps_count + 1;     
                 
                 % speed up/down
                 switch obj. steps_count
                     case {10, 20, 30, 50} 
%                          obj.kk = obj.kk-1;
                 end
             end            
        end
        
       
        
        function obj = finish(obj)
            %FINISH forces MJX simulation to stop and closes visualization          
           
            fprintf('%d steps\n',obj.steps_count);
            tempfig = figure('Name','Press to proceed','NumberTitle','off');
            waitforbuttonpress;
            close(tempfig);        
            mjx('exit');               
        end   
               
        
%         function obj = fivelinks_VC_MJX(inputArg1,inputArg2)
%             %FIVELINKS_VC_MJX Construct an instance of this class
%             %   Detailed explanation goes here
%             obj.l = inputArg1 + inputArg2;
%         end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
end

