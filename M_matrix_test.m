robot = quadpod_planar_VC;

robot.l =  [ 1; 1; 1; 1; 1; 1; 1; 1; 1 ];
robot.m = [ 25; 5; 5; 5; 5; 5; 5; 5; 5 ];
robot.r = 0.02;
robot.qa = zeros(9, 2);
robot = robot.COM_calcs();
robot = robot.J_calcs();

tic;
M = M_matrix(robot.J, robot.m, robot.c, robot.l, robot.qa);
toc;
